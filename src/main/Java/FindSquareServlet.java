import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/findSquare")
public class FindSquareServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        PrintWriter out = response.getWriter();

        Integer high = Integer.parseInt(request.getParameter("high"));
        Integer length = Integer.parseInt(request.getParameter("length"));
        int result = high * length;


        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet findSquare</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>" + "The square is" + result + "</h2>");
            out.println("</body></html>");
        } finally {
            out.close();
        }
//        request.getRequestDispatcher("/findSquare/index.html").forward(request,response);
//        response.setContentType("text/html");
    }
}
